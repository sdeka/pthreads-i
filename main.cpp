
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <iomanip>
#include <streambuf>
#include <unistd.h>

#include "CStopWatch.h"

/*-------------------------------------------------------------------*/
void* getSamples(void* nSamples) {
   
    long numSamples = (long)nSamples;
    double x, y;
    double* estimate = new double();
    int numInCircle = 0.00;

    for(int i=0; i< numSamples; i++){
        x = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;
        y = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;

        if(x*x + y*y <=1){
            numInCircle++;
        }
    }
    *estimate = (4.0*numInCircle/((double) numSamples));
    return (void*)estimate;
}  

int main() {
   long        threadMin, threadMax, threadStep;
   long        sampleMin, sampleMax, sampleStep;
   long        numTrials;
   pthread_t*  thread_handles; 
   CStopWatch  timer;
   void*       retValue;
   double      estimate;

   threadMin  = 10; 
   threadMax  = 100;
   threadStep = 10;

   sampleMin  = 1000000;
   sampleMax  = 10000000;
   sampleStep = 1000000;

   numTrials  = 10;
   
   thread_handles = new pthread_t[threadMax]; 

   for(int numThreads=threadMin; numThreads<threadMax; numThreads+=threadStep){

      for(long numSamples=sampleMin; numSamples < sampleMax; numSamples+=sampleStep){
         for(int curTrial=0; curTrial<numTrials; curTrial++){
            timer.startTimer();
            for (long thread = 0; thread < numThreads; thread++){
               pthread_create(&thread_handles[thread], NULL, getSamples, (void*) (numSamples/numThreads));  
            }

            estimate = 0;
            for (int thread = 0; thread < numThreads; thread++){
                pthread_join(thread_handles[thread], (void **)&retValue);            
                estimate += *(double*)retValue;
                delete (double*)retValue;
            }
            estimate /= numThreads;
            timer.stopTimer();

            std::cout << std::fixed << std::setprecision(4) << estimate << " " 
                      << std::setprecision(0) << numThreads << " " << numSamples << " " 
                      << std::setprecision(4)<< timer.getElapsedTime() << "\n";
         }
      }
   }

   delete thread_handles;

   return 0;
} 